<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EastTweet!</title>
    <style>
        li {
            display: inline;
        }
    </style>
</head>
<body>
	<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '161610161094989',
      xfbml      : true,
      version    : 'v2.11'
    });
    FB.AppEvents.logPageView();
    // ADD ADDITIONAL FACEBOOK CODE HERE
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

// Place following code after FB.init call.

function onLogin(response) {
  if (response.status == 'connected') {
    FB.api('/me?fields=first_name', function(data) {
      var welcomeBlock = document.getElementById('fb-welcome');
      welcomeBlock.innerHTML = 'Hello, ' + data.first_name + '!';
    });
  }
}

FB.getLoginStatus(function(response) {
  // Check login status on load, and if the user is
  // already logged in, go directly to the welcome message.
  if (response.status == 'connected') {
    onLogin(response);
  } else {
    // Otherwise, show Login dialog first.
    FB.login(function(response) {
      onLogin(response);
    }, {scope: 'user_friends, email'});
  }
});


</script>
 <script>    
    	function testAPI(){
    		console.log('Welcome! Fetching your information.... ');
            FB.api('/me', function (response) {
            	debugger;
                console.log('Successful login for: ' + response.name);                                               
                document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';                
            }, { scope: 'user_birthday' });            
            FB.api('/me', {fields: 'birthday'}, function(response) {            	  
            	  debugger;
            	  var dateString = response.birthday;
            	  var dateParts = dateString.split("/");
            	  var dateObject = new Date(dateParts[2], dateParts[0] - 1, dateParts[1]);            	 
                  document.getElementById('DOB').innerHTML = 'Your Birth day is : ' + dateObject.toString();
                  
                  // Calculate date difference between two dates 
                  var ageDifMs = Date.now() - dateObject.getTime();
                  var ageDate = new Date(ageDifMs); // miliseconds from epoch
                  document.getElementById('age').innerHTML = 'Your age is : ' + Math.abs(ageDate.getUTCFullYear() - 1970);                  
            })
    	}
    	
    	function statusChangeCallback(response){
    		console.log('Function : statusChangeCallback');
            console.log(response);            
            if (response.status === 'connected') {	// Logged into your app and Facebook.                
                testAPI();
            } else if (response.status === 'not_authorized') {	// The person is logged into Facebook, but not your app.
                document.getElementById('status').innerHTML = 'Please log into this app.';
            } else {
                // The person is not logged into Facebook, so we're not sure if
                // they are logged into this app or not.
                document.getElementById('status').innerHTML = 'Please log into Facebook.';
            }
    	}
    	</script>


<script>
// not being used currently
function postToFacebook() {
	var body = 'Reading JS SDK documentation';
	FB.api('/me/feed', 'post', { message: 'Hello' }, function(response) {
	  if (!response || response.error) {
	    alert(response.error);
	  } else {
	    alert('Post ID: ' + response.id);
	  }
	});
}
</script>


<h1>Welcome to EasyTweet</h1>








        


 <fb:login-button scope="public_profile,email,user_friends" onlogin="checkLoginState();">
 </fb:login-button>

    <ul>
        <li><a href="tweets.jsp">Tweet</a></li>
        <li><a href="friends.jsp">Friends</a></li>
        <li><a href="toptweets.jsp">Top Tweet</a></li>
    </ul>
</body>
</html>